/**
 * Job: iframe
 *
 * Expected configuration:
 *
    "iframe-Atlasboard": {
      "title": "Atlasboard in an iframe", // Optional
      "url": "http://atlasboard.bitbucket.org/",
      "zoom": "0.75" // Optional
    }
 */

module.exports = {
    onRun: function (config, dependencies, jobCallback) {
        jobCallback(null, {title: config.title, url: config.url, zoom: config.zoom});
    }
};